const suits = ["Hearts", "Spades", "Diamonds", "Clubs"];
const displayVals = ['Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace'];
var deck = new Array();
var card = {
    suit: '',
    displayVal: '',
    val: 0,
    cardId: 0
};
var cardId = 1;
var busted = false;
var isSoft;
var blackjack = false;
var dealerBlackjack = false;
var dealerScore = 0;
var playerScore = 0;


var myDeck = getDeck();
myDeck = shuffleCards(myDeck);
var player1 = new CardPlayer('Player');
var player2 = new CardPlayer('Dealer');
newDeal();
dealerShouldDraw(player2)
determineWinner(playerScore, dealerScore)

function determineWinner(pScore, dScore) {
    if (pScore > dScore) {
        console.log("Player Wins");
    }
    else if (pScore < dScore) {
        console.log("Dealer Wins");
    }
    else console.log("It is a tie");
}

function dealerShouldDraw(array) {
    console.log("The dealer's second card is the " + array.hand[1].displayVal + " of " + array.hand[1].suit);
    dealerScore = calcPoints(player2.hand);
    console.log("The dealer total is " + dealerScore);
    if (busted) {
        console.log("You busted, please select New Deal");
    }
    else if (blackjack) {
        console.log("You won, please select New Deal");
    }
    else if (dealerBlackjack) {
        console.log("The dealer won, please select new deal")
    }
    else if (dealerScore < 17) {
        while (dealerScore < 17) {
            player2.drawCard()
            console.log("The dealer's next card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
            dealerScore = calcPoints(player2.hand);
            console.log("Dealer total is " + dealerScore);
            if (dealerScore > 21) {
                console.log("Dealer busted - you win");
                busted = true;
            }
        }
    }
    else if (dealerScore == 17 && isSoft) {
        player2.drawCard()
        console.log("The dealer's next card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
        dealerScore = calcPoints(player2.hand);
        console.log("Dealer total is " + dealerScore);
        if (dealerScore > 21) {
            console.log("Dealer busted - you win");
            busted = true;
        }
    }
    else console.log("Dealer holds at " + dealerScore)
}

//player1 card drawing
function anotherCard() {
    if (busted) {
        console.log("You busted, please select New Deal");
    }
    else if (blackjack) {
        console.log("You won, please select New Deal");
    }
    else if (dealerBlackjack) {
        console.log("The dealer won, please select new deal")
    }
    else {
        player1.drawCard();
        console.log("Your next card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
        playerScore = calcPoints(player1.hand);
        console.log("Your total is " + playerScore);
        if (playerScore > 21) {
            console.log("You busted - you lose");
            busted = true;
        }
    }
}

//creates a first deal and determines if blackjack
function newDeal() {
    player1.drawCard();
    console.log("Your first card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
    player2.drawCard();
    console.log("The dealer's first card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
    player1.drawCard();
    console.log("Your second card is the " + drawnCard.displayVal + " of " + drawnCard.suit);
    player2.drawCard();
    var playerScore = calcPoints(player1.hand);
    console.log("Your total is " + playerScore);
    var dealerScore = calcPoints(player2.hand);
    if (playerScore == 21) {
        console.log("Blackjack you win")
        blackjack = true;
    }
    else if (dealerScore == 21) {
        dealerBlackjack = true;
        console.log("Dealer has Blackjack - you lose");
    }
}

//calculates score
function calcPoints(array) {
    let total = 0;
    let aceCount = 0;
    isSoft = false;
    for (i = 0; i < array.length; i++) {
        if (array[i].displayVal == "Ace") {
            aceCount++;
        }
        total = total + array[i].val;
    }
    while (total > 21 && aceCount >= 1) {
        for (let i = aceCount; i < 0; i--) {
            total = total - 10;
            aceCount--;
        }
    }
    if (aceCount > 0) {
        isSoft = true;
    }
    return total;
}

//creates deck
function getDeck() {
    for (i = 0; i < suits.length; i++) {
        for (j = 0; j < displayVals.length; j++) {
            if (j == 12) {
                val = 11;
            }
            else if (j > 8 && j < 12) {
                val = 10;
            }
            else val = j + 2;
            card = { suit: suits[i], displayVal: displayVals[j], val: val, cardId: cardId };
            deck.push(card);
            cardId++;
        }
    }
    return deck;
}

//shuffles deck
function shuffleCards(deck) {
    var cardToShuffle = deck.length;

    while (cardToShuffle) {
        replaceCard = Math.floor(Math.random() * cardToShuffle--);
        tempCard = deck[cardToShuffle];
        deck[cardToShuffle] = deck[replaceCard];
        deck[replaceCard] = tempCard;
    }
    return deck;
}
//creates player, hand, draws card
function CardPlayer(nameString) {
    this.name = nameString;
    this.hand = new Array();
    this.drawCard = function () {
        drawnCard = myDeck.pop();
        this.hand.push(drawnCard);
    }
}

