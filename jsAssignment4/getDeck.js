const suits = ["Hearts", "Spades", "Diamonds", "Clubs"];
const displayVals = ['Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace']
var deck = new Array();
var card;
var cardId = 1;


 var myDeck = getDeck();
 for (i = 0; i < myDeck.length; i++) {
     console.log(myDeck[i]);
 }


 function getDeck()
{
 for (i = 0; i <suits.length; i++) 
 {
     for (j = 0; j < displayVals.length; j++) 
     {
         if (j == 12) 
         {
             val = 11;
         }
         else if (j > 8 && j < 12)
         {
             val = 10;
         }
         else val = j + 2;

         card = {suit: suits[i], displayVal: displayVals[j], val: val, cardId: cardId};
         deck.push(card);
         cardId++;
     }
 }
 return deck;
}